// Copyright the PPS in Golang project contributors
// SPDX-License-Identifier: BSD-2-Clause

// +build linux

// Package pps - object oriented interface to the
// Pulse-Per-Second kernel support..
package pps

import (
//    "encoding/binary"
	"fmt"
	"os"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
)

const (
	// GSecConv - Two to the 32nd power - easier on the brain than bit banging.
	GSecConv = 4294967296

	// Jan1970 - Number of seconds to shim between NTP and Unix time.
	Jan1970 = 0x83aa7e80

	// LinuxPPS - signal we are using LinuxPPS
	LinuxPPS = 1

	// Version = "5.3.6"

	// should be enough...
	// MaxSources = 16

	// APIVers1 - we use API version 1
	APIVers1 = 1
	// APIVers - we use API version 1
	APIVers = APIVers1

	// MaxNameLen = 32

	// CaptureAssert - capture assert events
	CaptureAssert = 0x01
	// CaptureClear - capture clear events
	CaptureClear = 0x02
	// CaptureBoth - capture assert and clear events
	CaptureBoth = 0x03

	// OffsetAssert - apply compensation for assert event
	OffsetAssert = 0x10
	// OffsetClear - apply compensation for clear event
	OffsetClear = 0x20

	// EchoAssert - feed back assert event to output
	EchoAssert = 0x40
	// EchoClear - feed back clear event to output
	EchoClear = 0x80

	// CanWait - can we wait for an event?
	CanWait = 0x100
	// CanPoll - bit reserved for future use
	CanPoll = 0x200

	// TSFmtTSPEC - select timespec format
	TSFmtTSPEC = 0x1000
	// TSFmtNTPFP - select NTP format
	TSFmtNTPFP = 0x2000

	// KCHardPPS - hardpps() (or equivalent)
	KCHardPPS = 0
	// KCHardPPSPhase - hardpps() constrained to use a phase-locked loop
	KCHardPPSPhase = 1
	// KCHardPPSFreq - hardpps() constrained to use a frequency-locked loop
	KCHardPPSFreq = 2

	// TimeInvalid - used to specify timeout==NULL
	TimeInvalid = 1 << 0
)

// Magic numbers to avoid deeply nested ioctl wrappers
const (
	IoGetParams = uint(2148036769)
	IoSetParams = uint(1074294946)
	IoGetCap    = uint(2148036771)
	IoFetch     = uint(3221778596)
	IoKcBind    = uint(1074294949)
)

/*
 * New data structures
 */

// BindArgs - Pulse-Per-Second kernel binding storage
type BindArgs struct {
	Tsformat int32 // format of time stamps
	Edge     int32 // selected event type
	Consumer int32 // selected kernel consumer
}

// NtpFp - Pulse-Per-Second time storage member
type NtpFp struct {
	Integral   uint
	Fractional uint
}

// Nope - 12 or 24? byte placeholder
type Nope [3]uint64

// Timeu - Pulse-Per-Second time storage bytes
type Timeu [4]uint32

// InfoT - Pulse-Per-Second information data storage
type InfoT struct {
	AssertSequence uint32 // seq. num. of assert event
	ClearSequence  uint32 // seq. num. of clear event
	AssertTu       Timeu  // time of assert event
	ClearTu        Timeu  // time of clear event
	CurrentMode    uint32 // current mode bits
}

// ParamsT - Pulse-Per-Second parameters data storage
type ParamsT struct {
	APIVersion  int32 // API version #
	Mode        int32 // mode bits
	AssertOffTu Timeu // offset compensation for assert
	ClearOffTu  Timeu // offset compensation for clear
}

// Handle - Pulse-Per-Second interface data storage
type Handle struct {
	Fd     os.File // represents a PPS source
	Info   InfoT
	Params ParamsT
	Caps   uint32
	Bind   BindArgs
	FData  FDataT
}

// FdataT - Modifiers for handle.Fetch()
type FDataT struct {
	Info    InfoT
	TimeOut Timeu
}

// ToTime - Convert the pile of shorts into a time.Time
// Only works unit the end of the epoch 2206ish...
func (t Timeu) ToTime() time.Time {
	return time.Unix(int64(t[0]), int64(t[2]))
}

// SetDuration - Convert a duration into a pile of shorts
// Only works unit the end of the epoch 2206ish...
func (t Timeu) SetDuration(d time.Duration) {
	t[2] = uint32(d.Nanoseconds() % GSecConv)
	t[0] = uint32(d.Nanoseconds() / GSecConv)
}

// SetInt32s - Convert a couple shorts into a pile of shorts
// Only works unit the end of the epoch 2206ish...
func (t Timeu) SetInt32s(s, ns int32) {
	t[2] = uint32(ns)
	t[0] = uint32(s)
}

//
// The PPS API
//

// New - Create a new Pulse Per Second device handle, open the file,
// and verify it supports kernel Pulse Per Second interface.
func New(path string) (Handle, error) {
	var ret error
	var fd *os.File
	var source Handle

	fd, ret = os.Open(path)
	if ret != nil {
		return source, fmt.Errorf("unable to open device \"%s\" (%v)", path, ret)
	}
	source.Fd = *fd

	ret = source.Getparams()
	if ret != nil {
		return source, fmt.Errorf("cannot get parameters (%v)", ret)
	}

	ret = source.Getcap()
	if ret != nil {
		return source, fmt.Errorf("cannot get capabilities (%v)", ret)
	}
	return source, nil
}

// Detach - Close the existing Pulse Per Second device handle
func (handle *Handle) Detach() error {
	return handle.Fd.Close()
}

// Getparams - Get parameters for the Pulse Per Second device handle
func (handle *Handle) Getparams() error {
	return ioctl(handle.Fd, IoGetParams, uintptr(unsafe.Pointer(&handle.Params)))
}

// Getcap - Get capabilities for the Pulse Per Second device handle
func (handle *Handle) Getcap() error {
	return ioctl(handle.Fd, IoGetCap, uintptr(unsafe.Pointer(&handle.Caps)))
}

// Setparams - Set parameters for the Pulse Per Second device handle
func (handle *Handle) Setparams() error {
	return ioctl(handle.Fd, IoSetParams, uintptr(unsafe.Pointer(&handle.Params)))
}

// Fetch - Fetch Pulse Per Second information for the Pulse Per Second device handle.
func (handle *Handle) Fetch() error {
	if handle.FData.TimeOut[3] != 1 && handle.FData.TimeOut[3] != ^uint32(1) {
		handle.FData.TimeOut[3] = 1
	}
	return ioctl(handle.Fd, IoFetch, uintptr(unsafe.Pointer(&handle.FData)))
}

// Kcbind - Get/Set kernel consumer state for the Pulse Per Second device handle.
func (handle *Handle) Kcbind() error {
	return ioctl(handle.Fd, IoKcBind, uintptr(unsafe.Pointer(&handle.Bind)))
}

// ioctl - A beaten up rewritten copy of ioctl from x/sys/unix/syscall_linux.
func ioctl(fd os.File, req uint, arg uintptr) error {
	_, _, e1 := unix.Syscall(unix.SYS_IOCTL, uintptr(fd.Fd()),
		uintptr(req),
		uintptr(arg))
	if e1 != 0 {
		return fmt.Errorf(unix.ErrnoName(e1))
	}
	return nil
}
