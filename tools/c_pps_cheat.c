//  SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors
#include <sys/timepps.h>
#include <stdio.h>
#include <stdlib.h>

// Generate (possibly architecture dependent) magic numbers
// to avoid wrapping ioctl in six layers of glue functions.
void main(void) {
	printf("unsigned long is %ld bytes\n", 8 * (long)sizeof(unsigned long));
	printf("_IOR('p', 0xa1, struct pps_kparams *) -> %ld\n", _IOR('p', 0xa1, struct pps_kparams *));
	printf("_IOW('p', 0xa2, struct pps_kparams *) -> %ld\n", _IOW('p', 0xa2, struct pps_kparams *));
	printf("_IOR('p', 0xa3, int *) -> %ld\n", _IOR('p', 0xa3, int *));
	printf("_IOWR('p', 0xa4, struct pps_fdata *) -> %ld\n", _IOWR('p', 0xa4, struct pps_fdata *));
	printf("_IOW('p', 0xa5, struct pps_bind_args *) -> %ld\n", _IOW('p', 0xa5, struct pps_bind_args *));
}
