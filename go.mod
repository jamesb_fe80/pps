module gitlab.com/jamesb_fe80/pps

go 1.15

require (
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20200929083018-4d22bbb62b3c
)
