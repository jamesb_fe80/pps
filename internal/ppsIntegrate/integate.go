package main

// #include <sys/timepps.h>
// int size_params = sizeof(pps_params_t);
// int size_info = sizeof(pps_info_t);
// int size_timeu = sizeof(pps_timeu_t);
// int size_bindargs = sizeof(struct pps_bind_args);
// int size_caps = sizeof(int);
import "C"
import (
	"fmt"
	"os"
	"unsafe"
	"github.com/sirupsen/logrus"
	"gitlab.com/jamesb_fe80/pps/pkg/pps"
)



func valcomp(gString, cString string, gValue, cValue uint) int {
	if gValue != cValue {
		logrus.Error(fmt.Sprintf(
			"%s(%d) != %s(%d)",
			gString, gValue,
			cString, cValue))
		return 1
	}
	logrus.Info(fmt.Sprintf(
		"%s(%d) == %s(%d)",
		gString, gValue,
		cString, cValue))
	return 0
}

func sizecomp(gString, cString string, gValue, cValue uint) int {
	if gValue != cValue {
		logrus.Error(fmt.Sprintf(
			"len:%s(%d) != len:%s(%d)",
			gString, gValue,
			cString, cValue))
		return 1
	}
	logrus.Info(fmt.Sprintf(
		"len:%s(%d) == len:%s(%d)",
		gString, gValue,
		cString, cValue))
	return 0
}

func main() {
	var sizeParams pps.ParamsT
	var sizeInfo pps.InfoT
	var sizeTimeu pps.Timeu
	var sizeNope pps.Nope
	var failCount int
	var sizeBindArgs pps.BindArgs
	var handle pps.Handle
	if true {
		failCount += valcomp("IoFetch", "PPS_FETCH",
			pps.IoFetch, C.PPS_FETCH)
		failCount += valcomp("IoGetCap", "PPS_GETCAP",
			pps.IoGetCap, C.PPS_GETCAP)
		failCount += valcomp("IoGetParams", "PPS_GETPARAMS",
			pps.IoGetParams, C.PPS_GETPARAMS)
		failCount += valcomp("IoKcBind", "PPS_KC_BIND",
			pps.IoKcBind, C.PPS_KC_BIND)
		failCount += valcomp("IoSetParams", "PPS_SETPARAMS",
			pps.IoSetParams, C.PPS_SETPARAMS)
	}
	if true {
		failCount += sizecomp("BindArgs", "struct pps_bind_args",
			uint(unsafe.Sizeof(sizeBindArgs)),
			uint(C.size_bindargs))
		failCount += sizecomp("InfoT", "pps_info_t",
			uint(unsafe.Sizeof(sizeInfo)),
			uint(C.size_info))
		failCount += sizecomp("ParamsT", "pps_params_t",
			uint(unsafe.Sizeof(sizeParams)),
			uint(C.size_params))
		failCount += sizecomp("InfoT", "pps_info_t",
			uint(unsafe.Sizeof(sizeInfo)),
			uint(C.size_info))
		failCount += sizecomp("Timeu", "pps_timeu",
			uint(unsafe.Sizeof(sizeTimeu)),
			uint(C.size_timeu))
		failCount += sizecomp("Nope", "pps_timeu",
			uint(unsafe.Sizeof(sizeNope)),
			uint(C.size_timeu))
		failCount += sizecomp("Caps", "pps_timeu",
			uint(unsafe.Sizeof(handle.Caps)),
			uint(C.size_caps))
	}
	// failCount += valcomp()
	if failCount > 1 {
		logrus.Error(fmt.Sprintf("%d tests failed.", failCount))
	} else if failCount == 1 {
		logrus.Warn("A test failed.")
	} else {
		logrus.Info("No tests failed")
	}
	os.Exit(failCount)
}
