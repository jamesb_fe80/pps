package main

import (
	"fmt"
	"os"
	"time"

	"golang.org/x/sys/unix"
)

const (
	tIoCSetD = 0x5423
	nPPS     = 18
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: %s <ttyS>\n", os.Args[0])
	os.Exit(1)
}

func main() {
	var fd *os.File
	var ldisc = int(nPPS)
	var err error

	if len(os.Args) < 2 {
		usage()
	}

	fd, err = os.Open(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "open: %v", err)
		os.Exit(1)
	}

	_, _, e1 := unix.Syscall(unix.SYS_IOCTL, fd.Fd(),
		uintptr(tIoCSetD),
		uintptr(ldisc))
	if e1 != 0 {
		err = fmt.Errorf(unix.ErrnoName(e1))
		fmt.Fprintf(os.Stderr, "ioctl(tIoCSetD): %v", err)
		os.Exit(1)
	}

	time.Sleep(86400 * time.Second)

	os.Exit(0)
}
