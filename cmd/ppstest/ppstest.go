package main

import (
	"fmt"
	"os"
	"time"

//	"golang.org/x/sys/unix"
	"gitlab.com/jamesb_fe80/pps/pkg/pps"
)

func findSource(path string) (*pps.Handle, error) {
	var err error
	var offset, timeout pps.Timeu
	var handle pps.Handle

	offset.SetInt32s(0, 0)
	timeout.SetInt32s(3, 0)
	fmt.Printf("trying PPS source \"%s\"\n", path)

	// Try to open file, create PPS handle and get capabilities
	handle, err = pps.New(path)
	if err != nil {
		return &handle, fmt.Errorf("can not handle file")
	}

	if (handle.Caps & pps.CaptureAssert) == 0 {
		return &handle, fmt.Errorf("can not CAPTUREASSERT")
	}

	handle.Params.Mode |=  pps.CaptureAssert
	handle.Params.Mode |=  pps.TSFmtTSPEC
	handle.Params.Mode &= ^pps.TSFmtNTPFP

	if (handle.Params.Mode & pps.OffsetAssert) != 0 {
		handle.Params.Mode |= pps.OffsetAssert
		handle.Params.AssertOffTu = offset
		// handle.Params.AssertOffTu.TuTs().Nsec = 0
	}
	err = handle.Setparams()
	if err != nil {
		return &handle, fmt.Errorf("cannot set parameters (%v)", err)
	}

	return &handle, nil
}

func fetchSource(i int, handle *pps.Handle) error {
	var fData pps.FDataT
	var err error
	/*
		struct timespec timeout
		int ret

		// create a zero-valued timeout
		timeout.tv_sec = 3
		timeout.tv_nsec = 0
	*/
	for {
		fData = handle.FData
		if (handle.Params.Mode & pps.CanWait) != 0 { /* waits for the next event */
			fData.TimeOut.SetInt32s(3, 0)
			fData.TimeOut[3] = ^uint32(1)
		} else {
			fData.TimeOut[3] = uint32(1)
			time.Sleep(time.Second)
		}
		fData.Info.CurrentMode |= pps.TSFmtTSPEC
		handle.FData = fData
		err = handle.Fetch()
		if err == nil {
			break
		} else {
			if fmt.Sprintf("%v", err) == "" {
				continue
			} else {
				return err
			}
		}
	}

	assert := handle.FData.Info.AssertTu
	clear  := handle.FData.Info.ClearTu
	ctime := clear.ToTime()

// 	fmt.Printf("source %d - "+
// 		"assert %d.%09d, sequence: %d - "+
// 		"clear  %d.%09d, sequence: %d\n", i,
// 		handle.FData.Info.AssertTu.GetU().Sec,
// 		handle.FData.Info.AssertTu.GetU().Nsec,
// 		handle.FData.Info.AssertSequence,
// 		handle.FData.Info.ClearTu.GetU().Sec,
// 		handle.FData.Info.ClearTu.GetU().Nsec,
// 		handle.FData.Info.ClearSequence)
	fmt.Printf("source %d - "+
		"assert %5d@%9d.%09d - "+
		"clear %5d@%9d.%09d"+
		" - mode(0x%04x) -- %s %s\n", i,
		handle.FData.Info.AssertSequence,
		assert[0], assert[2],
		handle.FData.Info.ClearSequence,
		clear[0], clear[2],
		handle.FData.Info.CurrentMode,
		assert.ToTime().Format("2006/01/02T15:04:05Z0700"),
		ctime.Format("2006/01/02T15:04:05Z0700"),
	)
	return nil
}

func usage() {
	fmt.Fprintf(os.Stderr, "usage: %s <ppsdev> [<ppsdev> ...]\n", os.Args[0])
	os.Exit(1)
}

func main() {
	var num int
	var handle [4]*pps.Handle
	var i int
	var err error

	/* Check the command line */
	if len(os.Args) < 2 {
		usage()
	}
	for i = 1; i < len(os.Args) && i <= 4; i++ {
		handle[i-1], err = findSource(os.Args[i])
		if err != nil {
			fmt.Fprintf(os.Stderr, "The last error was '%v'.\n", err)
			os.Exit(1)
		}
	}

	num = i - 1
	fmt.Printf("ok, found %d source(s), now start fetching data...\n", num)

	/* loop, printing the most recent timestamp every second or so */
	for {
		for i = 0; i < num; i++ {
			err = fetchSource(i, handle[i])
			if err != nil {
				fmt.Fprintf(os.Stderr, "The last error was '%v'.\n", err)
				os.Exit(1)
			}
		}
	}

	for i = num; i >= 0; i-- {
		handle[i].Detach()
	}
	os.Exit(0)
}
