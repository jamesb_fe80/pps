package main

import (
	"fmt"
	"os"

	"gitlab.com/jamesb_fe80/pps/pkg/pps"
	"golang.org/x/sys/unix"
)

const (
	adjStatus  = 0x0010
	staPpsfreq = 0x0002
	staPpstime = 0x0004
)

/* variables for the command-line operations
 * 0 means no operation
 * 1 mean enable
 * 2 means disable

static int do_bind = 0;		// are we binding or unbinding?
static int do_setflags = 0;	// should we manipulate kernel NTP PPS flags?
static int opt_edge = PPS_CAPTURECLEAR;	// which edge to use?
static char *device
*/

func findSource(path string) (*pps.Handle, error) {
	var err error
	var handle pps.Handle

	fmt.Printf("trying PPS source \"%s\"\n", path)

	// Try to open file, create PPS handle and get capabilities
	handle, err = pps.New(path)
	if err != nil {
		return &handle, fmt.Errorf("can not handle file")
	}

	if (handle.Params.Mode & pps.CaptureAssert) == 0 {
		return &handle, fmt.Errorf("can not CAPTUREASSERT")
	}

	return &handle, nil
}

func bind(handle *pps.Handle, edge int) error {
	handle.Bind.Tsformat = pps.TSFmtTSPEC
	handle.Bind.Edge = edge
	handle.Bind.Consumer = pps.KCHardPPS
	return handle.Kcbind()
}

func unbind(handle *pps.Handle) error {
	handle.Bind.Tsformat = pps.TSFmtTSPEC
	handle.Bind.Edge = 0
	handle.Bind.Consumer = pps.KCHardPPS
	return handle.Kcbind()
}

func setFlags(o optparse) error {
	var tmx *unix.Timex
	var err error
	tmx.Modes = 0

	_, err = unix.Adjtimex(tmx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "adjtimex get failed: %v\n", err)
		return err
	}

	tmx.Modes = adjStatus
	tmx.Status |= (staPpsfreq | staPpstime)

	_, err = unix.Adjtimex(tmx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "adjtimex get failed: %v\n", err)
		return err
	}

	return nil
}
func unsetFlags(o optparse) error {
	var tmx *unix.Timex
	var err error
	tmx.Modes = 0

	_, err = unix.Adjtimex(tmx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "adjtimex get failed: %v\n", err)
		return err
	}

	tmx.Modes = adjStatus
	tmx.Status &= ^(staPpsfreq | staPpstime)

	_, err = unix.Adjtimex(tmx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "adjtimex get failed: %v\n", err)
		return err
	}

	return nil
}

func dieIn(str string, er error) {
	fmt.Fprintf(os.Stderr, "%s %s(): %v\n", os.Executable, str, er)
	os.Exit(1)
}

func usage() {
	fmt.Fprintf(os.Stderr,
		`Usage: %s [-bBfFac] <ppsdev>
Commands:
  -b   bind kernel PPS consumer
  -B   unbind kernel PPS consumer
  -f   set kernel NTP PPS flags
  -F   unset kernel NTP PPS flags
Options:
  -a   use assert edge
  -c   use clear edge (default)
`, os.Args[0])
	os.Exit(1)
}

type optparse struct {
	flagOne  bool
	idx      int
	length   int
	opt      int64
	bind     int
	setflags int
	edge     int
}

type optfunc func()

func (o *optparse) help() {
	usage()
}

func (o *optparse) dobind() {
	o.bind = 1
	o.flagOne = true
}

func (o *optparse) unbind() {
	o.bind = 2
	o.flagOne = true
}

func (o *optparse) dosetflag() {
	o.setflags = 1
	o.flagOne = true
}

func (o *optparse) unsetflag() {
	o.setflags = 2
	o.flagOne = true
}

func (o *optparse) assert() {
	o.edge = pps.CaptureAssert
	o.flagOne = true
}

func (o *optparse) clear() {
	o.edge = pps.CaptureClear
	o.flagOne = true
}

func main() {
	var err error
	var handle *pps.Handle
	var opt optparse
	var str string
	var options = map[string]optfunc{
		"-b": opt.dobind,
		"-B": opt.unbind,
		"-f": opt.dosetflag,
		"-F": opt.unsetflag,
		"-a": opt.assert,
		"-c": opt.clear,
		"-h": opt.help,
	}
	opt.length = len(os.Args)
	if opt.length < 2 {
		usage()
	}
	for opt.idx = 1; opt.idx < opt.length; opt.idx++ {
		fun, valid := options[os.Args[opt.idx]]
		if valid {
			fun()
		} else {
			str = os.Args[opt.idx]
			if !opt.flagOne {
				dieIn("main", fmt.Errorf("Invalid argument: %v", str))
			}
			handle, err = findSource(str)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Can not use \"%s\" as source. %v\n", err)
			}
			switch opt.bind {
			case 2:
				err = bind(handle, 0)
				if err != nil {
					dieIn("Unbind failed", err)
				}
				break
			case 1:
				err = bind(handle, opt.edge)
				if err != nil {
					dieIn("Bind failed", err)
				}
				break
			default:
			}
			switch opt.setflags {
			case 2:
				err = unsetFlags(opt)
				if err != nil {
					dieIn("Flags unset", err)
				}
				break
			case 1:
				err = setFlags(opt)
				if err != nil {
					dieIn("Flags set", err)
				}
				break
			default:
			}
		}
	}
}
