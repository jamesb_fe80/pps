// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors
package main

import (
	"fmt"
	"syscall"
)

const uSperS = 1e6 // microseconds per second

func dobump(bump int64) {
	var t1, t2, t3 syscall.Timeval
	var sec int

	fmt.Printf("Bumping clock by %d microseconds.\n", bump)
	err1 := syscall.Gettimeofday(&t1)
	t2 = t1

	if bump > 1e6 || bump < -1e6 {
		sec = bump / 1e6
		bump -= sec * 1e6
		t2.Sec += sec
	}
	//	bump *= 1e3

	t2.Usec += bump

	for t2.Usec >= uSperS {
		t2.Usec -= uSperS
		t2.Sec++
	}

	for t2.Usec <= 0 {
		t2.Usec += uSperS
		t2.Sec--
	}

	err2 := syscall.Settimeofday(&t2)
	err3 := syscall.Gettimeofday(&t3)

	// Defer printing so it doesn't distort timing.
	if err1 != nil {
		fmt.Printf("Couldn't get old time: %s\n", err1)
	} else {
		fmt.Printf("Was: %d.%06d\n", t1.Sec, t1.Usec)
	}
	if err2 != nil {
		fmt.Printf("Couldn't set time: %s\n", err2)
		fmt.Printf("Try: %d.%06d\n", t2.Sec, t2.Usec)
	} else {
		fmt.Printf("Set: %d.%06d\n", t2.Sec, t2.Usec)
	}
	if err3 != nil {
		fmt.Printf("Couldn't get new time: %s\n", err3)
	} else {
		fmt.Printf("Now: %d.%06d\n", t3.Sec, t3.Usec)
	}
}
