// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors
package main

import (
	"fmt"
	"os"
	"syscall"
)

// Adjust clock rate using adjtimex and print the result (in format?)
func dotickadj(mode string, newtick int64) {
	var txc syscall.Timex
	txc.Tick = int64(newtick)

	if newtick != 0 {
		if txc.Tick < 1 {
			fmt.Fprintf(os.Stderr, "ntpfrob: silly value for tick: %d\n", newtick)
			os.Exit(1)
		}
		txc.Modes = 0x4000 // Magic number from timex header
	} else {
		txc.Modes = 0
	}

	state, err := syscall.Adjtimex(&txc)

	if err != nil {
		fmt.Println("Error: ", err)
		fmt.Println("State: ", state)
	} else {
		if mode == "j" {
			fmt.Printf("{\"tick\":%d}\n", txc.Tick)
		} else {
			fmt.Printf("tick = %d\n", txc.Tick)
		}
	}
}
