// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors
package main

import (
	"fmt"
	"os"
	"sort"
	"time"
)

// Elements is the number of sample to take (down from seemingly arbitrary 800,002)
const Elements = 750001

// NSamples is the number of samplles to display in ranks except in raw mode
const NSamples = 10

// Calculate and return system colck jitter.
func dojitter(Mode string) {
	var buffer [Elements]int64
	var iance, mean int64
	var i int

	for i := 0; i < Elements; i++ { // try to lock array in ram and fail
		buffer[i] = 314159
	}
	for i := 0; i < Elements; i++ { // get raw timings
		buffer[i] = time.Now().UnixNano()
	}

	if Mode == "r" {
		for i := 0; i < (Elements - 1); i++ {
			fmt.Println(float64(buffer[i]) / 1.0e9)
		}
		os.Exit(0)
	}
	mean = (buffer[Elements-1] - buffer[0]) / (Elements - 1) // calculte mean difference

	for i := 1; i < (Elements); i++ { // change all members of array except last to deltas
		buffer[(i - 1)] = buffer[i] - buffer[i-1]
	}

	iance = 0
	for i := 0; i < (Elements - 1); i++ { // change all members of array except last to deltas
		buffer[i] -= mean
		iance += buffer[i] * buffer[i]
	}
	iance = iance / (Elements - 1) // calculte mean difference

	sort.Slice(buffer[0:(Elements-1)], func(i, j int) bool { return buffer[i] < buffer[j] })

	if Mode == "j" {
		fmt.Print("{\"Ended\": ", float64(buffer[(Elements-1)])/1.0e9)
		fmt.Print(", \"Mean (µs)\": ", float64(mean)/1.0e3, ", \"Low Rank (us)\": [")
		for i = 0; i < NSamples; i++ {
			fmt.Print(float64(buffer[i]) / 1.0e3)
			if i < (NSamples - 1) {
				fmt.Print(", ")
			}
		}
		fmt.Print("], \"High Rank (µs)\": [")
		for i = (Elements - NSamples - 2); i < (Elements - 2); i++ {
			fmt.Print(float64(buffer[i]) / 1.0e3)
			if i < (Elements - 3) {
				fmt.Print(", ")
			}
		}
		fmt.Println("], \"Jitter (µs)\": ", float64(iance)/1.0e3, "}")
	} else {
		fmt.Println("Ended:\t", float64(buffer[(Elements-1)])/1.0e9)
		fmt.Println("Mean (µs):\t", float64(mean)/1.0e3)
		fmt.Println("Jitter (µs):\t", float64(iance)/1.0e3)
		fmt.Println("Low Rank (µs):")
		for i = 0; i < NSamples; i++ {
			fmt.Println("\t", float64(buffer[i])/1.0e3)
		}
		fmt.Println("High Rank (µs):")
		for i = (Elements - NSamples - 2); i < (Elements - 2); i++ {
			fmt.Println("\t", float64(buffer[i])/1.0e3)
		}
	}
	os.Exit(0)
}
