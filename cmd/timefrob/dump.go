// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors
package main

import (
	"fmt"
	//	"os"
	"syscall"
)

// Dump the return of an Adjtimex call, in what format and
// whether to force dumping Pulse-Per-Second information.
func dodump(mode string, force bool) {
	var txc syscall.Timex
	var strung string
	end := 10

	txc.Modes = 0
	state, err := syscall.Adjtimex(&txc)

	if err != nil {
		fmt.Println("Error: ", err)
		fmt.Println("State: ", state)
	} else {
		if force || ((txc.Status & 6) != 0) {
			end = 18
		}
		if mode == "j" {
			fmt.Printf("{\"time\":%d.%06d", txc.Time.Sec, txc.Time.Usec)
			strung = ", \"%s\": %d"
		} else {
			fmt.Printf("time = %d.%06d\n", txc.Time.Sec, txc.Time.Usec)
			strung = "%s = %d\n"
		}
		sfields := []string{"time offset (ns)",
			"TAI offset",
			"frequency offset",
			"error max (µs)",
			"error est (µs)",
			"clock cmd status",
			"pll constant",
			"clock precision (µs)",
			"clock tolerance",
			"tick (µs)",
			"PPS frequency",
			"PPS jitter (ns)",
			"PPS interval (s)",
			"PPS stability",
			"PPS jitter limit exceed",
			"PPS calibration intervals",
			"PPS calibration errors",
			"PPS stabilitiy exceed"}

		ifields := []int64{
			txc.Offset,
			int64(txc.Tai),
			txc.Freq,
			txc.Maxerror,
			txc.Esterror,
			int64(txc.Status),
			txc.Constant,
			txc.Precision,
			txc.Tolerance,
			txc.Tick,
			txc.Ppsfreq,
			txc.Jitter,
			int64(txc.Shift),
			txc.Stabil,
			txc.Jitcnt,
			txc.Calcnt,
			txc.Errcnt,
			txc.Stbcnt}
		for i := 0; i < end; i++ {
			fmt.Printf(strung, sfields[i], ifields[i])
		}
		if mode == "j" {
			fmt.Println("}")
		}
	}
}
