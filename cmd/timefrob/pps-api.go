// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors

package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/jamesb_fe80/pps/pkg/pps"
)

func chew(handle pps.Handle) {
	var assert, clear time.Time
	var diff time.Duration
	clear = time.Unix(handle.Info.ClearTu.GetU().Sec, handle.Info.ClearTu.GetU().Nsec)
	assert = time.Unix(handle.Info.AssertTu.GetU().Sec, handle.Info.AssertTu.GetU().Nsec)
	diff = clear.Sub(assert)
	fmt.Printf("%ld.%09ld %ld.%09ld  %u %u %.9f\n",
		handle.Info.AssertTu.GetU().Sec,
		handle.Info.AssertTu.GetU().Nsec,
		handle.Info.ClearTu.GetU().Sec,
		handle.Info.ClearTu.GetU().Nsec,

		handle.Info.AssertSequence,
		handle.Info.ClearSequence,

		float64(diff.Seconds())+float64(diff.Nanoseconds())*1.0e-9,
	)
}

func dieIn(str string, er error) {
	fmt.Printf("timefrob %s(): %v\n", str, er)
	os.Exit(1)
}

func doppscheck(device string) {
	var handle pps.Handle
	var er error
	var olda, oldc uint

	if device == "" {
		device = "/dev/cuaa1"
	}
	handle, er = pps.New(device)
	if er != nil {
		dieIn("pps.New", er)
	}
	defer handle.Fd.Close()

	er = handle.Getcap()
	if er != nil {
		dieIn("pps.Getcap", er)
	}

	// handle.Params.Mode = pps.PPS_CaptureAssert | pps.PPS_EchoAssert;
	handle.Params.Mode = pps.CaptureBoth
	// handle.Params.Mode = pps.PPS_CaptureAssert;

	handle.Params.APIVersion = pps.APIVers

	er = handle.Setparams()
	if er != nil {
		dieIn("pps.Setparams", er)
	}

	for {
		er = handle.Fetch()
		if er != nil {
			dieIn("pps.Fetch", er)
		}

		if handle.Info.AssertTu.GetU().Sec == int64(olda) &&
			handle.Info.ClearTu.GetU().Sec == int64(oldc) {
			time.Sleep(100 * time.Millisecond)
			continue
		}

		chew(handle)
		olda, oldc = uint(handle.Info.AssertTu.GetU().Sec), uint(handle.Info.ClearTu.GetU().Sec)
	}
}
