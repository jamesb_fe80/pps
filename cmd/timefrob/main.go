// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors
package main

import (
	"fmt"
	"os"
	"strconv"
)

func usage(name string) {
	fmt.Fprintf(os.Stderr, `usage:
%s [ -A] [-b bump] [ -a tick ] [-c] [-d] [-D] [-e] [-j] [-r] [-_] [-h] [-?]
        -a tick    Set kernel tick
        -A         Display kernel tick
        -b bump    Bump clock by specified microseconds.
        -c         Compute and display clock jitter.
        -d         dump some adjtimex fields
        -D         dump most adjtimex fields forcing PPS inclusion
        -e         Measure clock precision.
        -j         Report in self-describing JSON.
        -r         Raw mode. Only applies to the jitter mode.
        -p ppsdev  Look for PPS pulses on a specified device.
        -_         Unset raw or JSON mode
        -?         Print usage
        -h         Print usage
`, name)
	os.Exit(1)
}

type optparse struct {
	gotOne bool
	mode   string
	idx    int
	length int
	opt    int64
}

type optfunc func() error

func (o *optparse) parseInt() (int64, error) {
	o.idx++
	return strconv.ParseInt(os.Args[o.idx], 10, 64)
}

func (o *optparse) json() error {
	o.mode = "j"
	return nil
}
func (o *optparse) raw() error {
	o.mode = "r"
	return nil
}
func (o *optparse) unset() error {
	o.mode = "_"
	return nil
}
func (o *optparse) help() error {
	usage(os.Args[0])
	return nil
}
func (o *optparse) adjtime0() error {
	dotickadj(o.mode, 0)
	return nil
}
func (o *optparse) adjtime() error {
	var err error
	var i int64
	i, err = o.parseInt()
	if err != nil {
		dieIn("strconv.ParseInt", err)
	}
	dotickadj(o.mode, i)
	return nil
}
func (o *optparse) dobump0() error {
	var err error
	var i int64
	i, err = o.parseInt()
	if err != nil {
		dieIn("strconv.ParseInt", err)
	}
	dobump(i)
	return nil
}
func (o *optparse) jitter() error {
	dojitter(o.mode)
	return nil
}
func (o *optparse) adjtimex() error {
	dodump(o.mode, false)
	return nil
}
func (o *optparse) adjtimexF() error {
	dodump(o.mode, true)
	return nil
}
func (o *optparse) precision() error {
	doprecision(o.mode)
	return nil
}
func (o *optparse) pps() error {
	o.idx++
	doppscheck(os.Args[o.idx])
	return nil
}

func main() {
	var opt optparse
	var options = map[string]optfunc{
		"-j": opt.json,
		"-r": opt.raw,
		"-_": opt.unset,
		"-?": opt.help,
		"-h": opt.help,
		"-A": opt.adjtime0,
		"-a": opt.adjtime,
		"-b": opt.dobump0,
		"-c": opt.jitter,
		"-d": opt.adjtimex,
		"-D": opt.adjtimexF,
		"-e": opt.precision,
		"-p": opt.pps,
	}
	opt.length = len(os.Args)
	for opt.idx = 1; opt.idx < opt.length; opt.idx++ {
		fun, valid := options[os.Args[opt.idx]]
		if valid {
			err := fun()
			if err != nil {
				dieIn("main", err)
			}
		} else {
			dieIn("main", fmt.Errorf("Invalid argument: %v", os.Args[opt.idx]))
		}
	}
	if opt.length < 2 {
		usage(os.Args[0])
		os.Exit(1)
	}
	os.Exit(0)
}
