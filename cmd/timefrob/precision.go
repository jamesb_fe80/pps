// SPDX-License-Identifier: BSD-2-Clause
// Copyright by the PPS in Golang project contributors

// TBH: I understand nothing of this code -- JB192

package main

import (
	"fmt"
	"os"
	"time"
)

// I understand where none of these came from...
const (
	defaultSysPrecision = -99
	DNSECS              = 1000000
	HUSECS              = (1024 * 1024)
	MINSTEP             = 200
	MAXSTEP             = 20000000
	MINLOOPS            = 5
	MAXLOOPS            = (HUSECS * 1024)
)

func defaultGetResolution() (out int) {
	minsteps := MINLOOPS // need at least this many steps
	var i int
	var tp time.Time
	var diff, last, val int64

	tp = time.Now()
	last = tp.UnixNano()
	for i = 1 - minsteps; i < MAXLOOPS; i++ {
		tp = time.Now()
		diff = tp.UnixNano() - last
		if diff < 0 {
			diff += DNSECS
		}
		if diff > MINSTEP {
			if minsteps <= 0 {
				minsteps--
				break
			}
		}
		minsteps--
		last = tp.UnixNano()
	}
	diff /= 1000 // step down to microseconds

	fmt.Fprintf(os.Stderr, "resolution = %d (µs)ec after %d loop(s)\n",
		diff, i)

	diff = (diff * 3) / 2
	if i >= MAXLOOPS {
		fmt.Fprintf(os.Stderr,
			"     (Boy this machine is fast ! %d loops without a step)\n",
			MAXLOOPS)
		diff = 1 // No STEP, so FAST machine
	}
	if i == 0 {
		fmt.Fprintf(os.Stderr,
			"     (The resolution is less than the time to read the clock -- Assume 1µs)\n")
		diff = 1 // time to read clock >= resolution
	}
	val = HUSECS
	for i = 0; val > 0; i-- {
		val = val >> 1
		if diff > val {
			return i
		}
	}
	return defaultSysPrecision
}

func defaultGetPrecision() (out int64) {
	var tp time.Time
	var i, diff, last, nsec, val int64

	nsec = 0
	val = MAXSTEP
	tp = time.Now()
	last = tp.UnixNano()
	for i = 0; i < MINLOOPS && nsec < HUSECS*1024; {
		tp = time.Now()
		diff = tp.UnixNano() - last
		last = tp.UnixNano()
		if diff < 0 {
			diff += DNSECS
		}
		nsec += diff
		if diff > MINSTEP {
			i++
			if diff < val {
				val = diff
			}
		}
	}
	val /= 1000 // step down to microseconds
	fmt.Fprintf(os.Stderr, "precision  = %d (µs)ec after %d loop(s)\n",
		val, i)
	if nsec >= HUSECS*1024 {
		fmt.Fprintf(os.Stderr, "     (Boy this machine is fast! nsec was %d)\n",
			nsec)
		val = MINSTEP // val <= MINSTEP; fast machine
	}
	for i = 0; diff > val; i-- {
		diff = diff >> 1
	}
	return i
}

// Calculate log2 precision and resolution of the system clock
//
// TBH: I understand nothing of this code.
func doprecision(mode string) {
	strung := "log2(resolution) = %d, log2(precision) = %d\n"
	if mode == "j" {
		strung = "{\"log2 of resolution\":%d, \"log2 of precision\":%d}\n"
	}
	fmt.Printf(strung, defaultGetResolution(), defaultGetPrecision())
}
