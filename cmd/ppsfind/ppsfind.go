// +build linux

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

const (
	basePath = "/sys/class/pps"
	sector   = 512
)

func dieIn(prevErr error) {
	fmt.Fprintf(os.Stderr, "The last error was '%v'.\n", prevErr)
	pc := make([]uintptr, 128)
	n := runtime.Callers(0, pc)
	if n > 0 {
		frames := runtime.CallersFrames(pc)
		for {
			frame, _ := frames.Next()
			if strings.Contains(frame.File, "runtime/") {
				continue
			}
			if frame.File == "" {
				break
			}
			fmt.Fprintf(os.Stderr, "'%s':%d -- %s()\n",
				frame.File, frame.Line, frame.Function)
		}
	}
	os.Exit(1)
}

func main() {
	var top *os.File
	var list []os.FileInfo
	var err error
	var found, index, length int
	var dPath, midFrag, nameCon, pathCon string
	var bit []byte
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "usage: %s <name>\n", os.Args[0])
	}

	top, err = os.Open(basePath)
	if err != nil {
		dieIn(err)
	}
	list, err = top.Readdir(90)
	if err != nil {
		dieIn(err)
	}
	top.Close()

	length = len(list)
	for index = 0; index < length; index++ {
		midFrag = list[index].Name()
		dPath = fmt.Sprintf("%s/%s/name", basePath, midFrag)
		bit, err = ioutil.ReadFile(dPath)
		if err != nil {
			dieIn(err)
		}
		nameCon = strings.Trim(string(bit), "\n\t ")
		if !strings.Contains(nameCon, os.Args[1]) {
			continue
		}
		bit, err = ioutil.ReadFile(fmt.Sprintf("%s/%s/path", basePath, midFrag))
		if err != nil {
			dieIn(err)
		}
		pathCon = strings.Trim(string(bit), "\n\t ")
		fmt.Printf("%s: name=%s path=%s\n", midFrag, nameCon, pathCon)
		found++
	}
	if found == 0 {
		fmt.Fprint(os.Stderr, "Sorry, didn't find anything.\n")
	}
}
